# Basset – asset collections for Laravel

[http://blog.legacyteam.info/2015/02/basset-asset-collections-laravel/](http://blog.legacyteam.info/2015/02/basset-asset-collections-laravel/)

Basset is a production level asset manager designed for performance and developer happiness.

Basset uses simple collections (or bundles) configuration for setup and has little to no boilerplate or plugins configuration code.

Basset will not be a tool which you will need to configure, re-confgure and fine tune for your project.

It uses **Gulp** and has only 3 basic commands:

```
// Watch mode
gulp basset:watch

// Build
gulp build

// Clean old builds
gulp clean

// Or shorter
gulp w
gulp b
gulp c
```

Even though Basset started with Laravel integration package, Basset can be used in any Gulp project.

Basset contains several caching and optimization strategies which enable build times for large collections to run in just a few seconds or mostly under a second.

Basset has already been used in production for years, and produces collections/bundles smaller then any other comparable tool.

By default you can easily use ES2016, CoffeeScript, SCSS, Less, CDN's, Custom Fonts, Bower dependencies, Browserify bundles, Livereload and much more.

## Installation

Install Basset with npm:

```
npm install gulp laravel-basset
```

## Usage

In your `Gulpfile.js`:

```
var basset = require('laravel-basset')

// Load from file
basset.loadFile('./collections.json')

// Or load literal config
basset.load({
	collections: {
		// ...
	}
})
```

In `collections.json` file or in your `Gulpfile.js` add:

```
// This is an example
{
	collections: {
		app: {
			css: [
				'dependencies.scss',
				'application.less',
				'theme.css'
			]
			js: [
				'dependencies.js',
				// You can use globs freely
				'components/*.{js,coffee}',
				'app.js'
			]
		}
	}
}
```

Basset has several defaiult load paths and you should add a folder with your collection name into them, so in this example we use **app** collection so Basset will attempt to load **app** files from paths:

```
assets/app
resources/assets/app

// Default load paths
assets
resources/assets
```

After adding files to those folders which are defined in collections configuration you can now run:

```
gulp build
```

This will build your files and put the built files into **public/builds** folder. These files can be loaded using our static file *Gulp injector* or **thelegacy/basset** Laravel integration package.

## Environment

Basset by default attempts to load **.env** file from your application and loads `NODE_ENV` or `APP_ENV` variables in order to detect environment which you are using.

By knowing your environment Basset can apply smart configuration which can optimize your respective environments. So production builds are very optimized and smaller, but development environment builds and watch mode are able to use options which are much more convenient for development.

But whenever you want to override environment, you can run **production** or **local** gulp tasks before Basset build command, so:

```
// Override environment to production
gulp production build

// Override environment to local
gulp local build
```

# Still needs be documented:

## Custom fonts
## Images
## Runtime Type Checking
## Livereload
## Environment Variables
## CDN's
## Bower


