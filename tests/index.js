var gulp = require('gulp')
var runSequence = require('run-sequence')
var tape = require('tape')
var fs = require('fs')
var Path = require('path')
var basset = require('../index.js')

var buildDir = 'builds';
var manifestFile = Path.join(buildDir, 'manifest.json')

tape('starts build', function(t) {
  basset.manifest = {}
  basset.config.dest = 'builds'
  basset.loadFile('./collections.json');
  runSequence('clean', 'build', t.end)
})

tape('should build manifest.json', function(t) {
  t.true(fs.existsSync(manifestFile), 'collection file should exist')
  t.deepEqual({
    'main': {
      'local': {
        'js': {
          'main/main.js': 'main/fixtures/main/main-c4238edcaf585158686a3ecf88f610d5d0246773.js'
        },
        'css': {}
      }
    },
    'environment': 'local'
  }, JSON.parse(fs.readFileSync(manifestFile)));
  t.end()
})

tape('builds files with hashed names', function(t) {
  var builtFile = Path.join(buildDir, "main/fixtures/main//main-c4238edcaf585158686a3ecf88f610d5d0246773.js");
  t.true(fs.existsSync(builtFile), 'main built file should exist');
  t.end()
})

tape('ads angular dependency injection', function(t) {
  basset.load({
    'basePaths': [
      'fixtures'
    ],
    'collections': {
      'ng': {
        'js': [
          'ng.js'
        ]
      }
    }
  });

  runSequence('production', 'build', function() {
    var contents = fs.readFileSync(Path.join(buildDir, 'ng-32600806d659e1c186de3b1bce4a385211023076.js'));

    t.equal('"use strict";var app=angular.module("ang",[]);app.controller("Controller",["$scope",function(o){console.log("works")}]),app.config(["$rootScope",function(o){console.log("works")}]),app.run(["$rootScope",function(o){console.log("works")}]);', contents.toString());

    t.end()
  });
})
