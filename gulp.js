var fs = require('fs')
var colors = require('chalk')

var replace
try {
  replace = require('gulp-replace-task')
} catch (error) {
  console.error('Please install ' + colors.cyan('gulp-replace-task') + ' module, run:')
  console.error('\nnpm install gulp-replace-task --save-dev\n')
  throw error
}

function collection (manifest, name, type, options) {
  var files
  var original
  var publicFile
  var html = ''

  var env = manifest.environment || process.env.APP_ENV || process.env.NODE_ENV || 'local'
  try {
    files = manifest[name][env][type]
  } catch (err) {
    files = {}
  }

  for (var original in files) {
    var path = files[original]
    if (path.indexOf('//') >= 0) {
      publicFile = path
    } else {
      publicFile = options['public'] + '/' + path
    }

    if (type === 'js') {
      html += '<script defer src="' + publicFile + '"></script>' + '\n'
    } else if (type === 'css') {
      html += '<link rel="stylesheet" href="' + publicFile + '"/>' + '\n'
    }
  }

  return function(wholeMatch, opening, contents, closing) {
    return opening+"\n"+html+closing
  }
}

function bassetHtmlInjector (options) {
  var manifest
  options.public = options.public || '/builds'
  options.manifest = options.manifest || 'public/builds/manifest.json'

  try {
    manifest = JSON.parse(fs.readFileSync(options.manifest))
  } catch (err) {}

  if (! manifest) {
    return
  }

  var patterns = []
  for (var name in manifest) {
    if (name == 'environment') {
      continue
    }

    patterns.push({
      match: new RegExp(
        '(<!--\\s*basset:' + name + ':css\\s*--\>)'+
        '([\\w\\W]*?)'+
        '(<!--\\s*basset:end\\s*--\>)'
      ),
      replacement: collection(manifest, name, 'css', options)
    })

    patterns.push({
      match: new RegExp(
        '(<!--\\s*basset:' + name + ':js\\s*--\>)'+
        '([\\w\\W]*?)'+
        '(<!--\\s*basset:end\\s*--\>)'
      ),
      replacement: collection(manifest, name, 'js', options)
    })
  }

  return replace({patterns: patterns})
}

module.exports = bassetHtmlInjector