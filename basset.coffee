bassetOptions = {
  verbose: false
  cache: false
}

do detectEnvironment = ->
  {argv} = require('optimist')
  require('dotenv').load silent: true

  detectedEnv = process.env.APP_ENV or process.env.NODE_ENV or 'local'
  if argv.production
    detectedEnv = 'production'
  process.env.NODE_ENV = detectedEnv

  if argv.verbose
    bassetOptions.verbose = true
  if argv.cache
    bassetOptions.cache = true

Path = require 'path'
memo = require 'fast-memoize'

loader = memo (module) ->
  if module is 'lo'
      m = require 'lodash'
  else if module is 'gulpif'
    m = require 'gulp-if'
  else if module is 'rename'
    m = require 'gulp-rename'
  else if module is 'runSequence'
    m = require 'run-sequence'
  else if module is 'streamify'
    m = require 'gulp-streamify'
  else if module is 'through'
    m = require 'through2'
  else if module is 'concat'
    m = require 'gulp-concat'
  else if module is 'newer'
    m = require 'gulp-newer'
  else if module is 'coffee'
    m = require 'gulp-coffee'
  else if module is 'less'
    m = require 'gulp-less'
  else if module is 'autoprefixer'
    m = require 'gulp-autoprefixer'
  else if module is 'cssmin'
    m = require 'gulp-cssmin'
  else if module is 'uglify'
    m = require 'gulp-uglify'
  else if module is 'sass'
    m = require 'gulp-sass'
  else if module is 'browserifyInc'
    m = require 'browserify-incremental'
  else if module is 'envify'
    m = require 'envify/custom'
  else if module is 'coffeeify'
    m = require 'coffee-reactify'
  else if module is 'livereload'
    m = require 'gulp-livereload'
  else if module is 'plumber'
    m = require 'gulp-plumber'
  else if module is 'ngAnnotate'
    m = require 'gulp-ng-annotate'
  else if module is 'Promise'
    m = require 'promise'
    require('promise/lib/rejection-tracking').enable(allRejections: true)
  else if module is 'notifier'
    m = require 'node-notifier'
  else if module is 'Hjson'
    m = require 'hjson'
  else if module is 'sourcemaps'
    m = require 'gulp-sourcemaps'
  else if module is 'applySourceMap'
    m = require 'vinyl-sourcemaps-apply'
  else if module is 'colors'
    m = require 'chalk'
  else if module is 'babel'
    m = require 'babel-core'
  else if module is 'typecheck'
    m = require('babel-plugin-typecheck').default
  else if module is 'babelInlineEnv'
    m = require 'babel-plugin-transform-inline-environment-variables'
  else if module is 'reactPreset'
    m = require 'babel-preset-react'
  else if module is 'babelDecorators'
    require('babel-template')
    m = require('babel-plugin-transform-decorators-legacy').default
  else if module is 'babelClassProperties'
    m = require 'babel-plugin-transform-class-properties'
  else if module is 'es2015Preset'
    {buildPreset} = require 'babel-preset-es2015'
    m = buildPreset {}, loose: true
    # cwd = process.cwd
    # process.cwd = -> __dirname
    # m = require 'babel-preset-es2015-loose'
    # process.cwd = cwd
  else
    m = require module
  return m

log = ->
  if bassetOptions.verbose
    console.log arguments...

only = (extensions...) ->
  through = loader 'through'

  return through.obj (file, enc, callback) ->
    if file.isNull()
      callback null, file
      return
    else if file.isStream()
      return throw new Error('Streams are not supported!')

    isIt = false
    for ext in extensions
      if Path.extname(file.path) is ext
        isIt = true
        break

    if isIt
      callback null, file
    else
      callback()

isRawFile = (file) ->
  return file.indexOf('//') is 0 or file.indexOf('://') > 0

excludeRawFiles = (files) ->
  f = []
  if files?.length > 0
    for file in files
      unless isRawFile file
        f.push file
  return f

hashify = ->
  through = loader 'through'
  return through.obj (file, enc, callback) ->
    if file.isNull()
      callback null, file
      return
    else if file.isStream()
      return throw new Error('Streams are not supported!')

    if file.shad?
      file.path = Path.join Path.dirname(file.path), file.shad
    callback null, file

logFile = (log, other...) ->
  through = loader 'through'
  return through.obj (file, enc, callback) ->
    if log
      if other
        console.log other..., file.path
      else
        console.log file.path

    callback null, file

normalizePath = memo (path) ->
  path.replace /\//g, Path.sep

removeDirs = (path, dirs) ->
  dirs.sort (a, b) ->
    al = a?.length or 0
    bl = b?.length or 0
    bl - al

  path = normalizePath path

  for dir in dirs
    dir = normalizePath dir

    if path.indexOf(dir) is 0
      path = path.replace(dir, '')
      break

  if path[0] is '/' or path[0] is '\\'
    path = path.substring(1)

  return path

fixSlashes = (path) ->
  regex = /\\/g
  path.replace regex, '/'

isLess = (file) ->
  Path.extname(file.path) is '.less'

isSass = (file) ->
  Path.extname(file.path) is '.scss'

isCoffee = (file) ->
  return Path.extname(file.path) is '.coffee'

checkExt = (path, extensions)->
  for ext in extensions
    if path.indexOf(ext) is path.length - ext.length
      return true
  false

isStyle = (path) -> checkExt path, ['.less', '.scss', '.coffee']

isScript = (path) -> checkExt path, ['.js', '.coffee', '.ts']

unrelativize = ->
  through = loader 'through'
  through.obj (file, enc, callback) ->
    regex = /\.\.[\\\/]/g
    match = file.relative.match regex
    if /^\.\.[\\\/]/g.test(file.relative) and match
      base = Path.resolve('.')
      for m in match
        base = Path.resolve base + '/..'
      file.base = base
    callback null, file

passNext = ->
  through = loader 'through'
  streamify = loader 'streamify'
  streamify through.obj (file, enc, callback) ->
    if file.isNull()
      callback null, file
      return
    else if file.isStream()
      return throw new Error('Streams are not supported!')
    callback null, file

isBowerComponent = (file) -> return file.indexOf('bower_components') >= 0

shad = (file, ext) ->
  crypto = loader 'crypto'
  shasum = crypto.createHash('sha1')
  shasum.update(file.contents)
  sha = shasum.digest('hex')
  file.originalPath = file.path + ''
  Path.basename(file.path, Path.extname(file.path)) + '-' + sha + '.' + ext

# normalizeDotPaths = (name) ->
normalizeDotPaths = ->
  through = loader 'through'
  through.obj (file, enc, callback) ->
    dirname = Path.dirname(file.path)
    extname = Path.extname(file.path)
    basename = Path.basename(file.path, extname)
    dirname = dirname.replace /\./g, '-'
    file.path = Path.join(dirname, basename) + extname
    callback null, file

expression = (step) ->
  streamify = loader 'streamify'
  gulpif = loader 'gulpif'
  use = step
  if step.if and step.then
    if step.else
      use = streamify gulpif(step.if, expression(step.then), expression(step.else))
    else
      use = streamify gulpif(step.if, expression(step.then))
  use


class TransformCache

  constructor: ->
    @saveCache = {}
    @manifestPath = ''

  load: (@manifestPath) ->
    lo = loader 'lo'
    fs = loader 'fs'
    @saveCache = lo.extend {}, try
      JSON.parse fs.readFileSync(@manifestPath)
    catch error
      {}

  save: ->
    if @dirty
      log "Writing transform cache"
      process.nextTick =>
        @cleanUp(@saveCache)
        fs = loader 'fs'
        fs.writeFile @manifestPath, JSON.stringify(@saveCache, null, "\t"), (err) ->
          handleError if err

  cleanUp: (saveCache) ->
    now = new Date().getTime()
    months_ago = now - 3*30*24*60*60*1000

    loopSingle = (name, multiple) ->
      for sha, def of multiple
        if def.timeStamp > months_ago
          delete multiple[sha]

    for name, multiple of saveCache
      loopSingle name, multiple

  notProcessed: (file) ->
    file._processed isnt true

  fill: (name) ->
    @saveCache[name] ?= {}
    through = loader 'through'
    streamify = loader 'streamify'
    crypto = loader 'crypto'

    streamify through.obj (file, enc, callback) =>
      if file.isNull()
        callback null, file
        return
      else if file.isStream()
        return throw new Error('Streams are not supported!')

      shasum = crypto.createHash('sha1')
      shasum.update(file.contents)
      sha = shasum.digest('hex')

      if @saveCache[name][sha]
        contents = @saveCache[name][sha]?.contents ? @saveCache[name][sha]

        file.contents = new Buffer(contents)
        file._processed = true

      file._sha = sha
      callback null, file

  add: (name) ->
    @saveCache[name] ?= {}
    through = loader 'through'
    streamify = loader 'streamify'

    streamify through.obj (file, enc, callback) =>
      if file.isNull()
        callback null, file
        return
      else if file.isStream()
        return throw new Error('Streams are not supported!')

      unless @saveCache[name][file._sha]
        @dirty = true
        @saveCache[name][file._sha] = {
          contents: file.contents.toString(enc)
          timestamp: new Date().getTime()
        }

      callback(null, file)


class Event
  constructor: (@data) ->
    @prevented = false

  preventDefault: ->
    @prevented = true

  isDefaultPrevented: -> @prevented is true


class Basset

  constructor: (options = {}) ->
    @lodash = options.lodash or do -> loader 'lo'
    @Hjson = options.Hjson or do -> loader 'Hjson'
    @livereload = options.Hjson or do -> loader 'livereload'
    EventEmitter = loader 'events'
    @events = new EventEmitter()
    @reset()

  reset: ->
    @collections = {}
    @bundles = {}
    @config = {
      dest: 'public/builds'
      manifest: 'manifest.json'
      basePaths: [
        'resources/assets'
        'assets'
      ]
    };
    @localEnvs = [
      'local'
      'dev'
      'development'
    ]
    @loadedDefinitions = []
    @pipes = {}
    @manifest = do =>
      manifestPath = Path.resolve @config.dest + '/' + @config.manifest
      return try
        require manifestPath
      catch error
        {}


  loadFile: (file, config = {}) ->
    fs = loader 'fs'
    Hjson = loader 'Hjson'

    @gulp = if config.gulp then config.gulp else loader 'gulp'
    collectionsJsonDefinition = Path.resolve file
    contents = fs.readFileSync(collectionsJsonDefinition).toString()
    json = Hjson.parse(contents)
    @scan(json, config, file)

  load: (config) ->
    passthrough = {}
    for option in ['filters']
      if config[option]?
        passthrough[option] = config[option]

    @gulp = if config.gulp then config.gulp else loader 'gulp'
    @scan(collections = config, passthrough, process.cwd() + '/Gulpfile')

  writeManifest: ->
    fs = loader 'fs'

    @manifest.environment = if @isProduction() then 'production' else 'local'
    json = JSON.stringify(@manifest, null, '  ')
    fs.writeFileSync @config.dest + '/' + @config.manifest, json

  findCachesPath: ->
    fs = loader 'fs'
    path = ''
    frameworkPath = 'storage/framework/cache'

    if fs.existsSync(frameworkPath)
      frameworkPath += Path.sep + 'basset'
      if not fs.existsSync(frameworkPath)
        fs.mkdirSync(frameworkPath)

    return if fs.existsSync(frameworkPath)
      Path.resolve(frameworkPath)
    else
      Path.resolve(@config.dest)

  putIntoManifest: (file, name, ext, callback) =>
    unless file.originalPath?
      throw new Error "File doesn't have a originalPath property #{file.path}"

    lo = loader 'lo'
    pathResolve = Path.resolve(file.originalPath)

    dirs = lo.extend [], @config.baseDirsRealpaths
    builds = Path.resolve @config.dest
    if @isProduction()
      dirs.push builds

    original = removeDirs pathResolve, dirs

    resolved = file.path.replace builds, ''

    env = if @isProduction() then 'production' else 'local'

    @manifest[name] ?= {}
    @manifest[name][env] ?= {}
    @manifest[name][env][ext] ?= []
    @manifest[name][env][ext][original] = resolved
    callback()

  finalizeManifest: (name, ext) ->
    through = loader 'through'
    lo = loader 'lo'
    return through.obj (file, enc, callback) =>
      if file.isNull()
        callback null, file
        return
      else if file.isStream()
        return throw new Error('Streams are not supported!')

      if lo.endsWith(file.path, '.map')
        callback null, file
        return

      @putIntoManifest file, name, ext, ->
        callback null, file

  isProduction: ->
    return process.env.NODE_ENV not in @localEnvs

  scan: (collectionsJson, configPassed = {}, collectionFile) ->
    pipes = @pipes
    lo = @lodash
    gulp = @gulp
    runSequence = loader('runSequence').use(gulp)
    self = this

    if collectionFile not in @loadedDefinitions
      @loadedDefinitions.push collectionFile

    @config = lo.extend(@config, configPassed)

    for name, definition of collectionsJson.collections
      @collections[name] = definition

    do (collectionsJson, collectionFile, Path) =>
      if collectionsJson.basePaths?
        for path in collectionsJson.basePaths
          path = Path.dirname(Path.resolve(collectionFile)) + '/' + path
          relativePath = './' + Path.relative Path.resolve('.'), path
          @config.basePaths.push relativePath
      else
        path = Path.dirname(Path.resolve(collectionFile))
        relativePath = './' + Path.relative Path.resolve('.'), path
        topLevel = relativePath is './'
        if not topLevel
          @config.basePaths.push relativePath

      @config.basePaths = lo.uniq @config.basePaths

    isProduction = @isProduction.bind(@)

    filters = @config.filters = lo.extend({
      less:
        paths: [Path.join(__dirname, 'less', 'includes')]

      livereload:
        port: process.env?.LIVERELOAD_PORT or 35729

      sourcemaps: {}

      babelify: ->
        options = plugins: [], presets: []

        options.presets.push loader 'es2015Preset'
        options.presets.push loader 'reactPreset'

        options.plugins.push loader 'babelDecorators'
        options.plugins.push loader 'babelInlineEnv'

        options.plugins.push loader 'babelClassProperties'

        unless isProduction()
          options.plugins.push loader 'typecheck'

        self.events.emit('babel:options')

        return options

      watchify:
        watch: false
        setup: (bundle) ->
          coffeeify = loader 'coffeeify'
          babelify = loader 'babelify'
          fs = loader 'fs'
          brfs = loader 'brfs'
          envify = loader 'envify'
          uglifyify = loader 'uglifyify'

          self.events.emit 'browserify:before-transforms', bundle

          bundle.transform(coffeeify)
          bundle.transform(babelify.configure(filters.babelify()))
          bundle.transform(brfs)

          self.events.emit 'browserify:after-transforms', bundle

          if isProduction()
            self.events.emit 'browserify:before-production-transforms', bundle

            sourceMap = checkSourceMaps() is true
            bundle.transform envify({_: 'purge'})
            options = {global: true, mangle: false, sourceMap}
            options.output = {
              comments: true
            }
            options.compress = {
              # angular: true
            }
            bundle.transform options, uglifyify

            self.events.emit 'browserify:after-production-transforms', bundle

          self.events.emit 'browserify:after-setup', bundle

          bundle

        extensions: '.coffee'

      autoprefixer:
        browsers: ['last 20 versions']
        cascade: true

      cssmin: {}

      uglify: {}

      sass: null

      ngAnnotate:
        remove: false
        add: true
        single_quotes: true
    }, @config.filters or {})

    CACHES_PATH = @findCachesPath()
    @config.baseDirsGlob =  '{' + @config.basePaths.join(',') + '}'
    @config.baseDirsRealpaths = []

    baseDirsGlob = @config.baseDirsGlob
    baseDirsRealpaths = @config.baseDirsRealpaths

    transformCache = null

    envCache = ->
      crypto = loader 'crypto'

      envCache.hash ?= crypto.createHash 'md5'
                             .update JSON.stringify process.env
                             .digest 'hex'
      envCache.hash


    initTransformCache = ->
      unless transformCache?
        envHash = envCache()
        transformCache = new TransformCache
        transformCache.load CACHES_PATH + "/basset-cache-#{envHash}.tmp"

    scanBasedirs = ->
      Promise = loader 'Promise'
      glob = loader 'glob'

      return scanBasedirs.scanPromise if scanBasedirs.scanPromise?
      scanBasedirs.scanPromise = new Promise (done) ->
        glob baseDirsGlob, (err, dirs) ->
          baseDirsRealpaths[k] = Path.resolve(v) for v, k in dirs
          done()

    checkSourceMaps = -> true if filters.sourcemaps?.write is true

    handleError = (err) ->
      for d in baseDirsRealpaths
        regex = new RegExp(d + '/', 'g')
        err.message = err.message.replace(regex, '')

      if bassetOptions.verbose or not filters.watchify.watch
        console.log err.message, err.stack
      else
        console.log err.message

      if err.extract?
        console.log err.extract.join("\n")

      if err._babel
        err.excerpt = err.codeFrame
        console.log err.excerpt

      message = err.message

      if err.extract
        message += "\n" + err.extract.join("\n")

      notifier = loader 'notifier'
      beeper = loader 'beeper'

      event = new Event(err)
      self.events.emit 'build:error', event

      unless event.isDefaultPrevented()
        notifier.notify(
          title: 'Basset - Build Error',
          message: message
          sound: true
          time: 30 * 1000
        , (err) ->
          beeper() if err
        )

        unless filters.watchify.watch
          e = new Event(err)
          self.events.emit('build:error-exit', e)

          unless e.isDefaultPrevented()
            process.exit(1)

        @emit?('end')
      return

    buildsCount = 0

    blockLivereload = false

    livereload = loader 'livereload'

    livereloadWildcard = =>
      if buildsCount and not blockLivereload
        event = new Event({path:'*'})
        @events.emit('reload', event)
        unless event.isDefaultPrevented()
          livereload.changed('*')

    livereloadPath = (path) =>
      if buildsCount and not blockLivereload
        event = new Event({path})
        @events.emit('reload', event)
        unless event.isDefaultPrevented()
          livereload.changed(path)

    debounceReload = lo.debounce livereloadWildcard, 150

    livereloadPipe = ->
      through = loader 'through'
      through.obj (file, enc, callback) ->
        if filters.watchify.watch
          if file.shad?
            debounceReload()
          else
            livereloadPath(file.path)
        callback null, file

    @events.on 'livereload:change', (args) ->
      livereload.changed args...

    prefixWithBase = (name, files) ->
      prefixed = []
      for f in files
        if f[0] is '!'
          for dir in baseDirsRealpaths
            prefixed.push '!' + dir + '/' + f.substring(1)
        else
          prefixed.push(baseDirsGlob + '/' + name + '/' + f)

      return prefixed

    swapWithMinFile = =>
      through = loader 'through'
      through.obj (file, enc, callback) =>
        if file.isNull()
          callback null, file
          return
        else if file.isStream()
          return throw new Error('Streams are not supported!')

        dirname = Path.dirname file.path
        extname = Path.extname file.path
        basename = Path.basename file.path, extname
        minFile = Path.join(dirname, basename) + '.min' + extname

        fs = loader 'fs'
        colors = loader 'colors'

        fs.exists minFile, (exists) =>
          return callback null, file if not exists

          console.log colors.cyan('Discovered'), removeDirs minFile, @config.baseDirsRealpaths

          fs.readFile minFile, (err, contents) ->
            return handleError(err) if err
            file.contents = contents
            callback null, file

    force = false

    globCheckSkipClean = (name, ext) =>
      through = loader 'through'
      return through.obj (file, enc, callback) =>
        if file.isNull()
          callback null, file
          return
        else if file.isStream()
          return throw new Error('Streams are not supported!')

        glob = loader 'glob'
        fs = loader 'fs'

        file.originalPath = file.path + ''

        if ext is 'css' and not isProduction()
          hashGlob = do =>
            fileWithStar = Path.dirname(file.path) + '/' + Path.basename(file.path, Path.extname(file.path)) + '.' + ext
            withoutRealpaths = removeDirs fileWithStar, baseDirsRealpaths
            return @config.dest + "/#{name}/#{baseDirsGlob}/" + withoutRealpaths


          glob hashGlob, {}, (err, files) =>
            exists = false
            files.forEach (filePath) ->
              exists = filePath
              return false

            if exists and force isnt true
              fs.stat exists, (err, stat) =>
                if err
                  return handleError err

                if file.stat.mtime.getTime() > stat.mtime.getTime()
                  callback null, file
                else
                  log 'Skiping', file.path

                  fakeFile = {
                    path: Path.resolve(exists)
                    originalPath: file.originalPath
                  }

                  @putIntoManifest fakeFile, name, ext, ->
                    callback()
            else
              callback null, file
          return


        if file.shad?
          throw new Error('Running shad twice?')

        file.shad = shad file, ext

        hashGlob = do =>
          fileWithStar = Path.dirname(file.path) + '/' + Path.basename(file.path, Path.extname(file.path)) + '-*' + '.{' + ext + ',' + ext + '.map}'

          dirs = lo.extend [], baseDirsRealpaths
          if isProduction()
            dirs.push Path.resolve(@config.dest)

          withoutRealpaths = removeDirs fileWithStar, dirs

          if isProduction()
            return @config.dest + '/' + withoutRealpaths

          return @config.dest + "/#{name}/#{baseDirsGlob}/" + withoutRealpaths

        regex = do =>
          fileWithStar = Path.dirname(file.path) + '/' + Path.basename(file.path, Path.extname(file.path)) + '-[0-9a-f]{5,40}' + '.(' + ext + '|' + ext+ '.map)$'

          dirs = lo.extend [], baseDirsRealpaths
          if isProduction()
            dirs.push Path.resolve(@config.dest)

          withoutRealpaths = removeDirs fileWithStar, dirs

          if isProduction()
            return @config.dest + '/' + withoutRealpaths
          return withoutRealpaths

        glob hashGlob, {}, (err, files) =>
          exists = false

          files.forEach (filePath) ->
            resolvedBase = Path.basename(filePath)

            if force isnt true and resolvedBase is file.shad
              log 'Skiping', file.path
              exists = filePath
            else
              expression = new RegExp(regex, 'gi')

              if expression.test filePath
                log 'Deleting', filePath
                fs.unlinkSync(filePath)

          if exists
            file.path = Path.resolve(exists)

            @putIntoManifest file, name, ext, ->
              callback()
          else
            callback null, file

    watchStreams = []

    modulesRegex = "(require(\\s+|\\()|import\\b|export\\b)"

    browserifyBundle = (name) =>
      through = loader 'through'
      return through.obj (file, enc, callback) =>
        if file.isNull()
          callback null, file
          return
        else if file.isStream()
          return throw new Error('Streams are not supported!')

        contents = file.contents.toString(enc)
        regex = new RegExp modulesRegex, 'g'

        if not regex.test contents
          file.contentsToString = contents
          return callback null, file

        file.isBundle = true

        file.originalPath = file.path + ''

        bundler = @bundles[file.path]

        unless bundler
          opts =
            debug: not isProduction()

          if checkSourceMaps()
            opts.debug = true

          envHash = envCache()
          browserifyCacheFile = CACHES_PATH + "/browserify-cache-#{name}.#{envHash}.tmp"

          browserify = loader 'browserify'
          browserifyInc = loader 'browserifyInc'

          browserifyOptions = {
            entries: [file.path]
            extensions: ['.coffee', '.json']
            cache: {}
            packageCache: {}
            debug: opts.debug
            # incremental options
            fullPaths: true
            cacheFile: browserifyCacheFile
          }

          self.events.emit 'browserify:options', browserifyOptions

          bundler = browserify(browserifyOptions)

          bundler.$meta = {}

          bundler = filters.watchify.setup(bundler)
          browserifyInc(bundler)

          if filters.watchify.watch is true
            watchify = loader 'watchify'
            bundler = watchify(bundler, ignoreWatch: true, poll: true)
            watchStreams.push bundler

            dirs = do ->
              for d in baseDirsRealpaths
                d + Path.sep + name
            overridePath = removeDirs file.path, dirs

            bundler.on 'update', =>
              colors = loader 'colors'
              process.nextTick =>
                dest = Path.resolve(@config.dest)
                console.log colors.cyan('Bundling'), removeDirs(file.path, [dest])

              watchifyOverrides[name] = [overridePath]
              buildTasks["build:#{name}:scripts"]().then ->
                delete watchifyOverrides[name]
              return

          bundler
            .on 'bundle', (output) =>
              output.on 'end', =>
                if filters.watchify.watch is true
                  @events.emit('browserify:bundle-end')
                  livereloadWildcard()
              return

          @bundles[file.path] = bundler

        file.contents = bundler.bundle().on 'error', handleError

        if filters.watchify.watch and not bundler.$meta.incremental
          bundler.$meta.incremental = true
          for path, value of bundler._options.cache
            bundler.emit('file', path)
        callback null, file


    isBundle = (file) -> file.isBundle is true

    squashBundleNames = ->
      fs = loader 'fs'
      gulpif = loader 'gulpif'
      through = loader 'through'
      streamify = loader 'streamify'

      gulpif checkSourceMaps, passNext(),
        gulpif isBundle,
          streamify through.obj (file, enc, callback) ->
            contents = file.contents.toString(enc)

            do ->
              regex = /"(\/[\w\W]+?)"/g
              res = contents.match regex

              i = 0
              references = {}
              if res and res.length > 0
                for string in res when string.length > 2
                  if fs.existsSync string.substring(1, string.length - 1)
                    if references[string]
                      contents = contents.replace string, references[string]
                    else
                      i++
                      contents = contents.replace string, i
                      references[string] = i

            file.contents = new Buffer(contents)
            callback null, file

    transformToES6 = () ->
      through = loader 'through'
      streamify = loader 'streamify'

      streamify through.obj (file, enc, callback) ->
        if file.isNull()
          callback null, file
          return
        else if file.isStream()
          return throw new Error('Streams are not supported!')

        if isBundle(file)
          return callback null, file

        if isBowerComponent(file.path)
          return callback null, file

        if file.coffee
          return callback null, file

        contents = file.contentsToString ?= file.contents.toString enc
        options = lo.extend {}, filters.babelify()

        if file.sourceMap
          options.inputSourceMap = file.sourceMap

        options.filename = file.path

        try
          babel = loader 'babel'
          result = babel.transform(contents, options)
        catch error
          @emit('error', error)
          return callback()

        transformed = result.code
        file.contents = new Buffer transformed, enc

        if file.sourceMap
          applySourceMap = loader 'applySourceMap'
          applySourceMap file, result.map

        callback null, file

    markAsCoffeeScript = ->
      through = loader 'through'
      through.obj (file, enc, callback) ->
        file.coffee = true
        callback null, file

    instructions = (p, instructionsList) ->
      for step in instructionsList
        if step.production
          if isProduction()
            p = p.pipe(expression(v)) for v in step.production
          else if step.local
            p = p.pipe(expression(v)) for v in step.local
        else
          p = p.pipe(expression(step))
      return p

    simpleUglify = {
      mangle: false
      compress:
        # angular: true,
        sequences: false,  # join consecutive statemets with the “comma operator”
        properties: false,  # optimize property access: a["foo"] → a.foo
        dead_code: false,  # discard unreachable code
        drop_debugger: false,  # discard “debugger” statements
        unsafe: false, # some unsafe optimizations (see below)
        conditionals: false,  # optimize if-s and conditional expressions
        comparisons: false,  # optimize comparisons
        evaluate: true,  # evaluate constant expressions
        booleans: false,  # optimize boolean expressions
        loops: false,  # optimize loops
        unused: false,  # drop unused variables/functions
        hoist_funs: true,  # hoist function declarations
        hoist_vars: false, # hoist variable declarations
        if_return: false,  # optimize if-s followed by return/continue
        join_vars: true,  # join var declarations
        cascade: false,  # try to cascade `right` into `left` in sequences
        side_effects: false,  # drop side-effect-free statements
        warnings: false,  # warn about potentially dangerous optimizations/code
    }

    bundleUglfy = lo.extend {}, simpleUglify, mangle: true

    pipes.scripts = (pipe, name) =>
      gulpif = loader 'gulpif'
      uglify = loader 'uglify'
      rename = loader 'rename'
      concat = loader 'concat'
      coffee = loader 'coffee'
      plumber = loader 'plumber'
      streamify = loader 'streamify'
      sourcemaps = loader 'sourcemaps'
      ngAnnotate = loader 'ngAnnotate'

      collectionDefition = @collections[name]

      doBrowserify = (file) ->
        if isBowerComponent file.path
          return false
        if collectionDefition.browserify?
          return collectionDefition.browserify is true
        return false

      pipe = instructions pipe, [
        plumber(errorHandler: handleError),
        only('.coffee', '.js'),
        gulpif(doBrowserify, browserifyBundle(name)),
        {
          production: [
            gulpif(isProduction, squashBundleNames()),
            gulpif(isBundle, passNext(), transformCache.fill(name)),
          ],
          local: [
            gulpif(isBundle, passNext(), globCheckSkipClean(name, 'js')),
          ]
        }
        streamify(sourcemaps.init(loadMaps: true)),
        {
          production: [
            gulpif(transformCache.notProcessed, streamify(swapWithMinFile())),
            gulpif(isCoffee, markAsCoffeeScript()),

            gulpif(transformCache.notProcessed, gulpif(isBundle, streamify(passNext()), gulpif(isCoffee, coffee(@config.coffee)))),

            gulpif(transformCache.notProcessed, gulpif(isCoffee, passNext(), transformToES6(name)))

            rename(extname: '.js'),

            gulpif(transformCache.notProcessed, streamify ngAnnotate(filters.ngAnnotate)),

            gulpif(transformCache.notProcessed, gulpif isBundle, streamify(uglify(bundleUglfy)), streamify(uglify(filters.uglify))),

            gulpif(isBundle, streamify(passNext()), transformCache.add(name)),

            streamify(concat(name+'.js', {newLine: "\n"})),

            streamify(rename(dirname: @config.dest)),
            streamify(globCheckSkipClean(name, 'js')),
          ]
          local: [
            {if: isCoffee, then: passNext(), else: transformToES6(name)}
            {if: isBundle, then: passNext(), else: {
              if: isCoffee, then: coffee(@config.coffee)
            }}
          ]
        }
        streamify(rename extname: '.js'),
        {
          production: [
            streamify(hashify()),
          ],
          local: [
            streamify(hashify()),
            streamify(normalizeDotPaths name),
          ]
        }
        streamify(unrelativize()),
        streamify(plumber.stop()),
        {
          production: [
            streamify(gulpif(checkSourceMaps, sourcemaps.write('.'))),
            squashBundleNames(),
            gulp.dest('.'),
          ],
          local: [
            streamify(sourcemaps.write(includeContent:true, sourceRoot: '/src')),
            streamify(gulp.dest(@config.dest + "/#{name}"))
          ]
        }
        streamify(@finalizeManifest name, 'js'),
        {
          if: isBundle, then: passNext(), else: livereloadPipe()
        }
      ]
      return pipe


    pipes.styles = (pipe, name) =>
      sass = loader 'sass'
      less = loader 'less'
      gulpif = loader 'gulpif'
      cssmin = loader 'cssmin'
      concat = loader 'concat'
      rename = loader 'rename'
      plumber = loader 'plumber'
      streamify = loader 'streamify'
      autoprefixer = loader 'autoprefixer'

      pipe = instructions pipe, [
        plumber(errorHandler: handleError),
        only('.less', '.css', '.scss'),
        {
          production: [
            transformCache.fill(name)
            {if: transformCache.notProcessed, then: swapWithMinFile()}
          ]
          local: [
            globCheckSkipClean(name, 'css')
          ]
        }
        {if: transformCache.notProcessed, then: {if: isSass, then: sass(@config.sass)}}
        {if: transformCache.notProcessed, then: {if: isLess, then: less(@config.less)}}
        {if: transformCache.notProcessed, then: autoprefixer(filters.autoprefixer)}
        {
          production: [
            {if: transformCache.notProcessed, then: cssmin(filters.cssmin)},
            transformCache.add(name),
            concat(name+'.css'),
            rename(dirname: @config.dest),
            globCheckSkipClean(name, 'css'),
            rename(dirname: '.')
          ]
          local: []
        },
        streamify(hashify()),
        streamify(normalizeDotPaths name),
        streamify(unrelativize()),
        plumber.stop(),
        {
          production: [
            gulp.dest(@config.dest + '/')
          ]
          local: [
            gulp.dest(@config.dest + "/#{name}")
          ]
        }
        @finalizeManifest(name, 'css'),
        streamify(livereloadPipe)
      ]
      return pipe

    bulkRunning = false

    bulkWatching = false

    overrides = {}

    watchifyOverrides = {}

    watchesTasks = {}

    buildTasks = {}

    startedWatches = []

    discoverFolderByFeature = (name, definition) ->
      Promise = loader 'Promise'
      fs = loader 'fs'
      return new Promise (resolvePromise) ->
        excludes = []

        walkDirTree = (dir) ->
          results = []
          entries = fs.readdirSync(dir)
          hasAsset = false
          entries.forEach (file) ->
            if isScript(file) or isStyle(file)
              hasAsset = true
              return false

          if not hasAsset
            excludes.push dir
          else
            entries.forEach (file) ->
              if file is 'bower_components'
                return

              file = dir + '/' + file
              stat = fs.statSync(file)
              if stat and stat.isDirectory()
                results.push file
                results = results.concat(walkDirTree(file))
              return

          return results

        glob = loader 'glob'
        glob baseDirsGlob + "/#{name}", (err, directories) ->
          [directory] = directories

          paths = walkDirTree directory
          paths = do ->
            p for p in paths when excludes.indexOf(p) is -1

          definition.js ?= []
          definition.css ?= []

          rest = '[^_]*.{js,coffee}'
          main = '{index,' + name + '}.{js,coffee}'

          if definition.browserify isnt on
            definition.js.push rest
            definition.js.push main

          rest = '[^_]*.{css,less,scss}'
          main = '{index,' + name + '}.{css,less,scss}'
          definition.css.push rest
          definition.css.push main

          for path in paths
            basename = Path.basename path
            corrected = path.substring directory.length + 1

            rest = corrected + '/[^_]*.{js,coffee}'
            main = corrected + '/{' + basename + ',index}.{js,coffee}'

            if definition.browserify isnt on
              definition.js.push rest
              definition.js.push main

            rest = corrected + '/[^_]*.{css,less,scss}'
            main = corrected + '/{' + basename + ',index}.{css,less,scss}'
            definition.css.push rest
            definition.css.push main

          resolvePromise()

    createTasks = (name, definition) =>
      taskName = "build:#{name}"

      definition.js ?= []
      definition.css ?= []

      unless Array.isArray(definition.js)
        throw "Collection #{name} js definition is not an array"

      unless Array.isArray(definition.css)
        throw "Collection #{name} css definition is not an array"

      if definition.browserify is undefined
        unless definition.discover
          definition.browserify = true

      if definition.js.length is 0
        definition.js = ["{index,main,#{name}}.{js,coffee}"]

      if definition.browserify and definition.js.length > 1
        yep = do ->
          for f in definition.js
            continue if isRawFile f
            f
        if yep.length > 1
          colors = loader 'colors'
          console.log colors.yellow('Warning'), "Browserify collections should have only one entry file (#{name} collection)"

      processScripts = =>
        Promise = loader 'Promise'
        return new Promise (resolve, reject) =>
          log taskName + ':scripts'

          if filters.watchify.watch and overrides[name]?.js and not definition.browserify
            files = overrides[name].js
          else if filters.watchify.watch and watchifyOverrides[name]
            files = watchifyOverrides[name]
          else
            files = excludeRawFiles @collections[name].js or []

          files = prefixWithBase name, files

          pipe = gulp.src files
          stream = pipes.scripts pipe, name
          stream.on 'finish', resolve
          stream.on 'error', reject
          stream

      buildTasks["build:#{name}:scripts"] = processScripts

      processFonts = =>
        Promise = loader 'Promise'
        if definition.fonts
          newer = loader 'newer'
          rename = loader 'rename'

          return new Promise (resolve, reject) =>
            log taskName + ':fonts'

            fonts = prefixWithBase name, definition.fonts
            stream = gulp.src fonts
            .pipe rename dirname: ''
            .pipe newer @config.dest + "/#{name}/fonts"
            .pipe gulp.dest @config.dest + "/#{name}/fonts"

            stream.on 'finish', resolve
            stream.on 'error', reject
        else
          return Promise.resolve true

      processCopy = =>
        Promise = loader 'Promise'
        return new Promise (done) =>
          return done(true) unless definition.copy

          scanBasedirs().then =>
            log taskName + ':copy'

            copyTasks = []
            promises = []

            for from, to of definition.copy then do (from, to) =>
              t = "#{taskName}:copy:" + from
              copyTasks.push t

              finalLocation = @config.dest + "/#{name}/#{to}"
              fileGlob = "#{baseDirsGlob}/#{name}/#{from}"

              dirs = lo.extend [], baseDirsRealpaths
              dirs.push d + "/#{name}/#{from}" for d, i in dirs

              through = loader 'through'
              renameDirectoryItems = through.obj (file, enc, callback) ->
                if file.isNull()
                  return callback null, file
                else if file.isStream()
                  return throw new Error('Streams are not supported!')

                file.originalPath = file.path + ''
                path = removeDirs file.path, dirs
                file.path = path

                callback null, file
                return

              promises.push new Promise (resolve) ->
                glob = loader 'glob'
                fs = loader 'fs'
                rename = loader 'rename'
                newer = loader 'newer'

                glob fileGlob, (err, files) ->
                  files.forEach (v) ->
                    fs.stat v, (err, stat) ->
                      if err
                        return handleError err

                      if stat.isFile()
                        gulp.task t, ->
                          gulp.src fileGlob
                          .pipe rename dirname: ''
                          .pipe newer finalLocation
                          .pipe gulp.dest finalLocation
                        resolve()

                      if stat.isDirectory()
                        gulp.task t, ->
                          gulp.src fileGlob + '/**/*'
                          .pipe renameDirectoryItems
                          .pipe newer finalLocation
                          .pipe gulp.dest finalLocation
                        resolve()

            Promise.all(promises).then ->
              runSequence copyTasks...
              done()

      processRaw = =>
        Promise = loader 'Promise'
        return new Promise (done) =>
          for ext in ['css', 'js'] then do (ext) =>
            files = @collections[name][ext]
            env = if isProduction() then 'production' else 'local'

            addRaw = (name, key, value) =>
              @manifest[name] ?= {}
              @manifest[name][env] ?= {}
              @manifest[name][env][ext] ?= {}
              @manifest[name][env][ext][key] = value

            for file, i in files when isRawFile file
              addRaw(name, file, file)
          done()

      processStyles = =>
        Promise = loader 'Promise'
        return new Promise (resolve, reject) =>
          log taskName + ':styles'

          if filters.watchify.watch and overrides[name]?.css
            files = overrides[name].css
          else
            files = excludeRawFiles @collections[name].css or []

          files = prefixWithBase name, files

          pipe = gulp.src files
          stream = pipes.styles pipe, name
          stream.on 'finish', resolve
          stream.on 'error', reject
          stream

      processBower = =>
        Promise = loader 'Promise'
        return new Promise (done) =>
          log taskName + ':bower'

          options = bowerJson: definition.bower or 'bower.json'
          bowerGlob = baseDirsGlob + '/' + name + '/' + options.bowerJson

          glob = loader 'glob'
          wiredep = loader 'wiredep'
          glob bowerGlob, {}, (err, files) =>
            delete options.bowerJson
            options.cwd = Path.dirname Path.resolve files[0]
            try
              results = wiredep options
            catch error
              error.message += " in collection #{name} - #{definition.bower}"
              handleError error
              return done()

            removeDirsMap = (v) -> removeDirs v, collectionDirs

            for ext in ['css', 'js']
              found = results[ext]

              if found
                collectionDirs = []
                collectionDirs.push("#{d}/#{name}") for d in baseDirsRealpaths
                found = found.map removeDirsMap
                combined = []

                # Bower dependencies after raw files
                addedFound = false

                if @collections[name][ext]? and @collections[name][ext].length > 0
                  for f in @collections[name][ext]
                    if not isRawFile(f) and not addedFound
                      combined = combined.concat found
                      addedFound = true
                    combined.push f
                else
                  combined = found

                @collections[name][ext] = combined

            done()

      minimatchRegexes = {}

      reorderManifest = =>
        Promise = loader 'Promise'
        return new Promise (done) =>
          log "reorder-manifest-#{name}"

          env = if isProduction() then 'production' else 'local'

          for ext in ['css', 'js'] then do (ext) =>
            files = @collections[name][ext]
            built = @manifest[name]?[env]?[ext] or do ->
              colors = loader 'colors'
              console.warn colors.yellow('Warning'), 'No', ext, 'files for', name+'?'
              {}

            ordered = {}

            for file in files
              if isRawFile(file)
                ordered[file] = file
              else
                for k, v of built
                  relativeFilename = removeDirs k, baseDirsRealpaths
                  relativeFilename = fixSlashes relativeFilename

                  if file.indexOf(baseDirsGlob + '/') is 0
                    match = name + '/' + file
                  else
                    match = name + '/' + file

                  minimatch = loader 'minimatch'
                  regex = minimatchRegexes[match] ?= minimatch.makeRe match

                  if relativeFilename.match(regex)
                    v = removeDirs v, baseDirsRealpaths
                    v = fixSlashes v
                    ordered[relativeFilename] = v

            @manifest[name] ?= {}
            @manifest[name][env] ?= {}
            @manifest[name][env][ext] ?= {}

            if isProduction()
              mainFile = name + '.' + ext
              @manifest[name][env][ext] ?= {}
              builtPath = @manifest[name][env][ext][mainFile]

              if builtPath
                relativeFilename = removeDirs builtPath, baseDirsRealpaths
                relativeFilename = fixSlashes relativeFilename
                ordered[mainFile] = relativeFilename

            @manifest[name][env][ext] = ordered
          done()

      watchesTasks["watch:#{name}:start"] = (done) =>
        startedWatches.push name

        files = []
        files = files.concat excludeRawFiles @collections[name].js or []
        files = files.concat excludeRawFiles @collections[name].css or []
        files = prefixWithBase name, files

        if definition.bower?
          defaults = bowerJson: 'bower.json'
          options = lo.extend(defaults, definition.bower)
          bowerGlob = baseDirsGlob + "/#{name}/" + options.bowerJson
          stream = gulp.watch bowerGlob, ['build']
          watchStreams.push stream

        livereload = loader 'livereload'

        filters.watchify.watch = true
        livereload.listen(filters.livereload)

        do (name) =>
          watches = prefixWithBase name, excludeRawFiles @collections[name].watch or []
          delete overrides[name]

          s = gulp.watch(watches).on 'change', ->
            runSequence ['force-build'], ['build:' + name]

          watchStreams.push s


        s2 = gulp.watch(files).on 'change', (event) =>
          return if force

          overrides[name] ?= {css: [], js: []}
          overrides[name].css = ['__']
          overrides[name].js = ['__']

          eventPath = event.path

          dirs = do ->
            for d in baseDirsRealpaths
              d + Path.sep + name

          overridePath = removeDirs eventPath, dirs

          if isScript(eventPath)
            overrides[name].js = [overridePath]
          else
            overrides[name].css = [overridePath]

          if not @bundles[eventPath]
            fin = ->
            ignore = {
              bower: true
              copy: true
              fonts: true
            }

            if isScript(eventPath)
              ignore.css = true
              buildCollection fin, ignore
            else if isStyle(eventPath)
              ignore.js = true
              buildCollection fin, ignore
            else
              gulp.start 'build:' + name

        watchStreams.push s2

        done()

      gulp.task "watch:#{name}", (done) ->
        tasks = []
        if not bulkWatching
          tasks.push ["build:#{name}"]

        scanBasedirs()
          .then ->
            if definition.discover
              discoverFolderByFeature(name, definition)
            else
              Promise = loader 'Promise'
              Promise.resolve true
          .then ->
            fn1 = ->
              watchesTasks["watch:#{name}:start"]( -> )

              if not buildsCount and not bulkRunning
                buildsCount++

              if not bulkRunning
                watchGit()

              done()

            if not bulkWatching
              runSequence ["build:#{name}"], fn1
            else
              fn1()

          .catch (err) ->
            console.error err
          return

      buildCollection = (done, ignore = {}) ->
        initTransformCache()

        Promise = loader 'Promise'

        scanBasedirs().then ->
          if definition.bower and not ignore.bower
            processBower()
          else
            Promise.resolve true
        .then ->
          if definition.discover
            discoverFolderByFeature(name, definition)
          else
            Promise.resolve true
        .then ->
          mainTasks = []

          if not ignore.css
            mainTasks.push processStyles()

          mainTasks.push processRaw()

          if definition.js and not ignore.js
            mainTasks.push processScripts()

          if definition.fonts and not ignore.fonts
            mainTasks.push processFonts()

          if definition.copy and not ignore.copy
            mainTasks.push processCopy()

          return Promise.all(mainTasks)
        .then ->
          reorderManifest()
        .catch (err) ->
          handleError err
        .then ->
          if bulkRunning
            done()
          else
            saveManifest().then ->
              done()
        return

      gulp.task taskName, buildCollection

    createTasks(name, definition) for name, definition of @collections

    saveManifest = =>
      Promise = loader 'Promise'
      return new Promise (resolve) =>
        log 'basset:save-manifest'
        force = false
        @writeManifest()
        @events.emit('save:manifest', @config.manifest)
        transformCache.save()
        resolve()

    closeAllWatches = (closeLivereloadServer = false) ->
      for s in watchStreams
        if typeof s.close is 'function'
          s.close()
        else
          s.end()

      watchStreams = []

      if closeLivereloadServer
        livereload = loader 'livereload'
        livereload.server.close()

    watchGit = =>
      return if watchGit.metaRunning

      watchGit.metaRunning = true

      s = gulp.watch([
        './.git/HEAD'
        './.git/refs/heads/**/*'
      ]).on 'change', =>
        console.log 'Detected Git change'

        overrides = {}

        closeAllWatches()

        watchGit.metaRunning = false

        blockLivereload = true

        restartTasks = []

        if bulkWatching
          restartTasks.push ['basset:watch']
        else
          for w in startedWatches
            restartTasks.push "watch:#{w}"

        del = loader 'del'
        del([@config.dest + '/' + @config.manifest], force: true).then =>
          log baseDirsRealpaths

          loadedDefinitions = lo.cloneDeep @loadedDefinitions
          @reset()

          for f in loadedDefinitions
            @loadFile f
            colors = loader 'colors'
            console.log colors.cyan('Loading'), f

          blockLivereload = false
          runSequence restartTasks...
        .catch handleError

      watchStreams.push s

    gulp.task 'basset:watch', (done) =>
      bulkWatching = true
      ts = Object.keys(@collections).map (v) -> "watch:#{v}"
      ts.unshift 'build'

      watchGit()

      scanBasedirs().then ->
        runSequence ts, ->
          done()
        return
      return

    gulp.task 'force-build', -> force = true

    gulp.task 'basset:clean', =>
      paths = [
        @config.dest + '/*' + if isProduction() then '' else '*/*.*'
        '!' + @config.dest + '/.gitignore'
      ]

      if bassetOptions.cache
        paths = [
          CACHES_PATH + Path.sep + '*.tmp'
        ]
      else
        paths.push '!' + CACHES_PATH + Path.sep + '*.tmp'

      log 'Deleting', paths
      through = loader 'through'
      gulp.src(paths, read: false)
        .pipe through.obj (file, enc, callback) ->
          del = loader 'del'
          del(file.path, {force:true}).then ->
            callback()

    gulp.task 'basset:build', (done) =>
      colors = loader 'colors'
      bulkRunning = true
      tasks = Object.keys(@collections).map (v) -> "build:#{v}"

      console.log colors.cyan('Building'), 'for', process.env.NODE_ENV

      scanBasedirs().then =>
        runSequence tasks, ->
          bulkRunning = false

          if not buildsCount
            buildsCount++

          saveManifest().then ->
            done()
          return
      return

    gulp.task 'build', ['basset:build']
    gulp.task 'clean', ['basset:clean']
    gulp.task 'b', ['basset:build']
    gulp.task 'w', ['basset:watch']
    gulp.task 'c', ['basset:clean']

    gulp.task 'production', ->
      process.env.NODE_ENV = 'production'

    gulp.task 'local', ->
      process.env.NODE_ENV = 'local'

module.exports = new Basset
module.exports.Basset = Basset
